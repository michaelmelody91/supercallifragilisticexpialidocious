package com.michaelmelody.metricsservice.exceptions;

import org.springframework.http.HttpStatus;

public class RejectedMetricReadingSubmissionException extends ApiResponseException{
    public RejectedMetricReadingSubmissionException() {
        super(HttpStatus.BAD_REQUEST, "Invalid submission");
    }
}
