package com.michaelmelody.metricsservice.repositories;

import com.michaelmelody.metricsservice.entities.MetricReading;
import org.springframework.data.repository.CrudRepository;

public interface MetricReadingRepository extends CrudRepository<MetricReading, Integer>, MetricReadingRepositoryCustom {
}
