package com.michaelmelody.metricsservice.repositories;

import com.michaelmelody.metricsservice.dtos.SensorMetricsSummary;
import com.michaelmelody.metricsservice.dtos.MetricsSummaryQuery;

import java.util.List;

public interface MetricReadingRepositoryCustom {

    List<SensorMetricsSummary> getMetricsSummary(MetricsSummaryQuery query);
}
