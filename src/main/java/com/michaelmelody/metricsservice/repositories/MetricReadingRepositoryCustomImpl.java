package com.michaelmelody.metricsservice.repositories;

import com.michaelmelody.metricsservice.dtos.SensorMetricsSummary;
import com.michaelmelody.metricsservice.dtos.MetricsSummaryQuery;
import com.michaelmelody.metricsservice.entities.MetricReading;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class MetricReadingRepositoryCustomImpl implements MetricReadingRepositoryCustom{

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<SensorMetricsSummary> getMetricsSummary(MetricsSummaryQuery metricsSummaryQuery) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<SensorMetricsSummary> query = cb.createQuery(SensorMetricsSummary.class);
        Root<MetricReading> root = query.from(MetricReading.class);
        query.multiselect(root.get("sensorId"),
                        cb.avg(root.get(("temperature"))),
                        cb.avg(root.get(("humidity"))))
                .groupBy(root.get("sensorId"))
                .where(cb.between(root.get("date"), metricsSummaryQuery.from, metricsSummaryQuery.to));;
        return entityManager.createQuery(query).getResultList();
    }
}
