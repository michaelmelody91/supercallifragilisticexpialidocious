package com.michaelmelody.metricsservice.dtos;

public enum Statistic {
    AVG, MIN, MAX
}
