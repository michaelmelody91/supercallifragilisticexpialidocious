package com.michaelmelody.metricsservice.dtos;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class MetricsSummaryQuery {
    public List<Integer> sensorIds;
    public Date from;
    public Date to;

    public MetricsSummaryQuery(List<Integer> sensorIds, Date from, Date to){
        initialiseProperties(sensorIds, from, to);
    }

    private void initialiseProperties(List<Integer> sensorIds, Date from, Date to) {
        initialiseSensorIds(sensorIds);
        initialiseFrom(from);
        initialiseTo(to);
    }

    private void initialiseSensorIds(List<Integer> sensorIds) {
        if(sensorIds == null) {
            this.sensorIds = Arrays.asList();
        }
    }
    private void initialiseFrom(Date from) {
        this.from = from;
        if(this.from == null) {
            this.from = new DateTime().minusDays(7).toDate();
        }
    }

    private void initialiseTo(Date to) {
        this.to = to;
        if(this.to == null) {
            this.from = new Date();
        }
    }
}
