package com.michaelmelody.metricsservice.dtos;

import com.michaelmelody.metricsservice.exceptions.RejectedMetricReadingSubmissionException;

/**
 * Treating this class as a data structure rather than an object.
 * As this is a DTO, I'm content with avoiding the use of getters/setters.
 */
public class MetricReadingSubmission {
    public Integer sensorId;
    public double temperature;
    public double humidity;
    public String date;
    public MetricReadingSubmission(Integer sensorId, double temperature, double humidity, String date) {
        if(!metricReadingIsValid(humidity)) {
            throw new RejectedMetricReadingSubmissionException();
        }
        this.sensorId = sensorId;
        this.temperature = temperature;
        this.humidity = humidity;
        this.date = date;
    }

    private boolean metricReadingIsValid(double humidity) {
        return humidity <= 100.0 && humidity >= 0.00;
    }
}
