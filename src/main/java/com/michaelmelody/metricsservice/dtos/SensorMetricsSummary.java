package com.michaelmelody.metricsservice.dtos;

public class SensorMetricsSummary {

    public Integer sensorId;
    public double avgTemperature;
    public double avgHumidity;

    public SensorMetricsSummary(Integer sensorId, double avgTemperature, double avgHumidity) {
        this.sensorId = sensorId;
        this.avgTemperature = avgTemperature;
        this.avgHumidity = avgHumidity;
    }
}
