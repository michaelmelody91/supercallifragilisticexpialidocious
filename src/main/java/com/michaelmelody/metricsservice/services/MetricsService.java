package com.michaelmelody.metricsservice.services;

import com.michaelmelody.metricsservice.dtos.MetricReadingSubmission;
import com.michaelmelody.metricsservice.dtos.SensorMetricsSummary;
import com.michaelmelody.metricsservice.dtos.MetricsSummaryQuery;
import com.michaelmelody.metricsservice.exceptions.RejectedMetricReadingSubmissionException;

import java.util.List;

public interface MetricsService {
    void submitMetricReading(MetricReadingSubmission metricReadingSubmission) throws RejectedMetricReadingSubmissionException;
    List<SensorMetricsSummary> getMetricsSummary(MetricsSummaryQuery query);
}