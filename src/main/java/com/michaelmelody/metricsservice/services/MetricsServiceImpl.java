package com.michaelmelody.metricsservice.services;

import com.michaelmelody.metricsservice.dtos.MetricReadingSubmission;
import com.michaelmelody.metricsservice.dtos.SensorMetricsSummary;
import com.michaelmelody.metricsservice.dtos.MetricsSummaryQuery;
import com.michaelmelody.metricsservice.entities.MetricReading;
import com.michaelmelody.metricsservice.exceptions.RejectedMetricReadingSubmissionException;
import com.michaelmelody.metricsservice.repositories.MetricReadingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MetricsServiceImpl implements MetricsService{

    private MetricReadingRepository metricReadingRepository;

    @Autowired
    public MetricsServiceImpl(MetricReadingRepository readingRepository) {
        this.metricReadingRepository = readingRepository;
    }

    @Override
    public void submitMetricReading(MetricReadingSubmission metricReadingSubmission) throws RejectedMetricReadingSubmissionException {
        metricReadingRepository.save(new MetricReading(metricReadingSubmission));
    }

    @Override
    public List<SensorMetricsSummary> getMetricsSummary(MetricsSummaryQuery query) {
        return metricReadingRepository.getMetricsSummary(query);
    }
}
