package com.michaelmelody.metricsservice.controllers;

import com.michaelmelody.metricsservice.dtos.MetricReadingSubmission;
import com.michaelmelody.metricsservice.dtos.MetricsSummaryQuery;
import com.michaelmelody.metricsservice.dtos.SensorMetricsSummary;
import com.michaelmelody.metricsservice.exceptions.ApiResponseException;
import com.michaelmelody.metricsservice.services.MetricsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/metrics")
public class MetricsController {

    Logger logger = LoggerFactory.getLogger(MetricsController.class);
    private MetricsService metricsService;
    @Autowired
    public MetricsController(MetricsService metricsService) {
        this.metricsService = metricsService;
    }

    @PostMapping()
    public void acceptMetricReading(@RequestBody MetricReadingSubmission metricReadingSubmission) throws ApiResponseException {
        logger.info("Metric reading submitted");
        metricsService.submitMetricReading(metricReadingSubmission);
    }

    @PostMapping("/summary")
    public List<SensorMetricsSummary> provideMetricsSummary(@RequestBody MetricsSummaryQuery metricsSummaryQuery) throws ApiResponseException {
        logger.info("Sensor metrics summary requested");
        return metricsService.getMetricsSummary(metricsSummaryQuery);
    }
}
