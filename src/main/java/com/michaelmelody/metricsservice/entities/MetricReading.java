package com.michaelmelody.metricsservice.entities;

import com.michaelmelody.metricsservice.dtos.MetricReadingSubmission;
import com.michaelmelody.metricsservice.exceptions.RejectedMetricReadingSubmissionException;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class MetricReading {

    public MetricReading(MetricReadingSubmission metricReadingSubmission) throws RejectedMetricReadingSubmissionException {
        this.humidity = metricReadingSubmission.humidity;
        this.temperature = metricReadingSubmission.temperature;
        this.sensorId = metricReadingSubmission.sensorId;
        try {
            this.date = Date.valueOf(metricReadingSubmission.date);
        } catch (IllegalArgumentException e) {
            throw new RejectedMetricReadingSubmissionException();
        }
    }
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private Integer sensorId;

    private double temperature;

    private Date date;

    private double humidity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSensorId() {
        return sensorId;
    }

    public void setSensorId(Integer sensorId) {
        this.sensorId = sensorId;
    }

    public Date getTimestamp() {
        return date;
    }

    public void setTimestamp(Date timestamp) {
        this.date = timestamp;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }
}