package com.michaelmelody.metricsservice.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.michaelmelody.metricsservice.dtos.MetricReadingSubmission;
import com.michaelmelody.metricsservice.dtos.MetricsSummaryQuery;
import com.michaelmelody.metricsservice.exceptions.RejectedMetricReadingSubmissionException;
import com.michaelmelody.metricsservice.services.MetricsService;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MetricsControllerTests {

    private static final String DUMMY_DATE = "2022-01-01";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MetricsService metricsService;

    @Test
    public void acceptMetricReading() throws Exception {
        // Given
        MetricReadingSubmission metricReadingSubmission = new MetricReadingSubmission(1, 0.0, 0.0, DUMMY_DATE);

        // When & Then
        this.mockMvc.perform(post("/api/metrics")
                .content(asJsonString(metricReadingSubmission))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        ArgumentCaptor<MetricReadingSubmission> argument = ArgumentCaptor.forClass(MetricReadingSubmission.class);
        verify(metricsService).submitMetricReading(argument.capture());
        assertThat(argument.getValue().temperature).isEqualTo(metricReadingSubmission.temperature);
        assertThat(argument.getValue().sensorId).isEqualTo(metricReadingSubmission.sensorId);
        assertThat(argument.getValue().humidity).isEqualTo(metricReadingSubmission.humidity);
    }

    @Test
    public void returnsBadRequestIsSubmissionIsRejected() throws Exception {
        // Given
        doThrow(new RejectedMetricReadingSubmissionException())
                .when(metricsService)
                .submitMetricReading(any());

        // When & Then
        this.mockMvc.perform(post("/api/metrics")
                        .content(asJsonString(new MetricReadingSubmission(1, 0.0, 0.0, DUMMY_DATE)))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void ifInvalidHumidityIsProvided() throws Exception {
        // Given
        MetricReadingSubmission metricReadingSubmission =
                new MetricReadingSubmission(1, 0.0, 20.0, DUMMY_DATE);
        metricReadingSubmission.humidity = 200.0;
        // When & Then
        this.mockMvc.perform(post("/api/metrics")
                        .content(asJsonString(metricReadingSubmission))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void provideMetricsSummary() throws Exception {
        // Given

        // When & Then
        this.mockMvc.perform(post("/api/metrics/summary")
                        .content(asJsonString(new MetricsSummaryQuery(null, null, null)))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
