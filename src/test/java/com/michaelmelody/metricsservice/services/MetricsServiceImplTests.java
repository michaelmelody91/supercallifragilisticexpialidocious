package com.michaelmelody.metricsservice.services;

import com.michaelmelody.metricsservice.dtos.MetricReadingSubmission;
import com.michaelmelody.metricsservice.dtos.MetricsSummaryQuery;
import com.michaelmelody.metricsservice.dtos.SensorMetricsSummary;
import com.michaelmelody.metricsservice.repositories.MetricReadingRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

public class MetricsServiceImplTests {

    MetricReadingRepository metricReadingRepository;

    private MetricsService metricsService;
    @BeforeEach
    public void setup() {
        metricReadingRepository = Mockito.mock(MetricReadingRepository.class);
        this.metricsService = new MetricsServiceImpl(metricReadingRepository);
    }

    @Test
    public void validMetricReadingSubmissionAcceptedTest(){
        // Given
        MetricReadingSubmission metricReadingSubmission =
                new MetricReadingSubmission(1, 15, 10.1, "2022-01-01");

        // When
        metricsService.submitMetricReading(metricReadingSubmission);

        // Then
        Mockito.verify(metricReadingRepository).save(any());
    }

    @Test
    public void sensorMetricsSummariesReturnedTest() {
        // Given
        MetricsSummaryQuery metricsSummaryQuery = new MetricsSummaryQuery(Arrays.asList(1),new Date(), new Date());
        when(metricReadingRepository.getMetricsSummary(eq(metricsSummaryQuery)))
                .thenReturn(Arrays.asList(new SensorMetricsSummary(1, 10.0, 15.0)));

        // When
        List<SensorMetricsSummary> sensorMetricsSummaries = metricsService.getMetricsSummary(metricsSummaryQuery);

        //Then
        Mockito.verify(metricReadingRepository).getMetricsSummary(eq(metricsSummaryQuery));
        assertThat(sensorMetricsSummaries.get(0).sensorId).isEqualTo(1);
        assertThat(sensorMetricsSummaries.get(0).avgTemperature).isEqualTo(10.0);
        assertThat(sensorMetricsSummaries.get(0).avgHumidity).isEqualTo(15.0);

    }
}
