package com.michaelmelody.metricsservice.dtos;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class SensorMetricsSummaryQueryTests {

    @Test
    public void queryIsInitialisedWithDefaultPropsNoneProvidedTest() {
        // Given
        MetricsSummaryQuery query = new MetricsSummaryQuery(null, null, null);

        // Then
        assertThat(query.sensorIds).isEqualTo(Arrays.asList());
        // TODO: Date testing == pain!
    }
}
