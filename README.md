## To run DB

````
cd ./src/main/resources
docker-compose up
````
## To run service
````
mvn spring-boot:run
````

## API

- [OpenAPI](/src/main/resources/api-docs/openapi.yaml)
- [Postman collection](/src/main/resources/api-docs/postman-collection.json)

## TODO

- I've not implemented the selection of which statistics to return (as in selecting a subset of statistics)
- Added no tests for the DAO layer
